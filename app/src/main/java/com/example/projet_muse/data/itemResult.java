package com.example.projet_muse.data;

import java.util.List;
import java.util.Map;

public class itemResult {
    public final boolean isLoading;
    public final  Map<String, ItemResponse> items;
    public final Throwable error;


    public itemResult(boolean isLoading,  Map<String, ItemResponse> items, Throwable error) {
        this.isLoading = isLoading;
        this.items = items;
        this.error = error;
    }
    public  Map<String, ItemResponse> getmyitem() {
        return this.items;
    }
}
