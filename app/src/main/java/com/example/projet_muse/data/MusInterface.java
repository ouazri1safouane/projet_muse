package com.example.projet_muse.data;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MusInterface {
    @Headers("Accept: application/json")
    @GET("/cerimuseum/collectionversion")
    Call<Float> getvERSION();
    @GET("/cerimuseum/categories")
    Call<List<String>> getCategoris();
    @GET("/cerimuseum/collection")
    Call < Map < String , ItemResponse > > getCollection();

}
