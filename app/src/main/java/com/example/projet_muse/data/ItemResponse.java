package com.example.projet_muse.data;

import java.util.List;

public class ItemResponse {
    public  int id;
    public  String name;
    public  List<String> categories;
    public  List<String> technicalDetails;
    public  String description;
    public  String brand;
    public  int year;
    public  List<Integer> timeFrame;
    public  Boolean working;


    public ItemResponse(String name, List<String> categories,  String description, String brand, int year, Boolean working) {
        this.name = name;
        this.categories = categories;
        this.technicalDetails = technicalDetails;
        this.description = description;
        this.brand = brand;
        this.year = year;
        this.timeFrame = timeFrame;
        this.working = working;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return this.id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public String getCategorie() {
        return this.categories.toString();
    }
     public void setTechnicalDetails(List<String> technicalDetails) {
         this.technicalDetails = technicalDetails;
     }
     public List<String> getTechnicalDetails() {
         return this.technicalDetails;
     }
     public void setTimeFrame(List<Integer> timeFrame) {
         this.timeFrame = timeFrame;
     }
     public List<Integer> getTimeFrame() {
         return this.timeFrame;
     }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return this.description;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getBrand() {
        return this.brand;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return this.year;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }
    public Boolean getWorking() {
        return this.working;
    }


}
