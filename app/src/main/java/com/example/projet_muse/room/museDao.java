package com.example.projet_muse.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.projet_muse.data.ItemResponse;
import com.example.projet_muse.room.version;

import java.util.List;

@Dao
public interface museDao {
    @Insert
    void insertVer(version version);

    @Query("SELECT * FROM versions Limit 1")
    version findVersion();
    @Query("Delete  FROM versions")
    void deleteAllVersion();

    @Insert
    void insetCatego(categories categorie);

    @Query("SELECT * FROM categories")
    LiveData<List<categories>> finallCatego();
    @Query("SELECT * FROM categories")
    List<categories> finallCategol();

    @Query("Delete  FROM categories")
    void deleteAllcatego();
    @Insert
    void insertItem(itemCollection itemCollection);

    @Query("SELECT itemId,name,categories,description,brand,year,working,technicalDetails,timeFrame FROM itemCollection")
    LiveData<List<itemCollection>> findallItem();

    @Query("SELECT itemId,name,categories,description,brand,year,working,technicalDetails,timeFrame FROM itemCollection")
    List<itemCollection> findallIteml();

    @Query("SELECT * FROM itemCollection WHERE itemId = :id")
    itemCollection finditem(int id);

    @Query("Delete  FROM itemCollection")
    void deleteAllItems();
}
