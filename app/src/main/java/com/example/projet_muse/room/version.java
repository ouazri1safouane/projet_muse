package com.example.projet_muse.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "versions")
public class version {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "versionId")
    private int id;
    @ColumnInfo(name = "versionVersion")
    private float Version ;
    public version(float Version ) {
        this.id = id;
        this.Version = Version;

    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public float getVersion() {
        return this.Version;
    }
    public void setVersion(float Version) {
        this.Version = Version;
    }

}
