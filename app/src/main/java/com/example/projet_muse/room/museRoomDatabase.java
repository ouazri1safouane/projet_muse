package com.example.projet_muse.room;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
@Database(entities = {version.class,categories.class,itemCollection.class}, version = 5)
public abstract class museRoomDatabase extends RoomDatabase {
    public abstract museDao museDao();

    private static volatile museRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static museRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (museRoomDatabase.class) {
                if (INSTANCE == null) {

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    museRoomDatabase.class,"muse_database").allowMainThreadQueries().fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

}
