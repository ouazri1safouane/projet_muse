package com.example.projet_muse.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.projet_muse.data.Categorie;

import java.util.List;
@Entity(tableName = "itemCollection")
public class itemCollection {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "itemId")
    public  int id;
    @ColumnInfo(name = "name")
    public  String name;
    @ColumnInfo(name = "categories")
    public String Categorie;
    @ColumnInfo(name = "technicalDetails")
    public  String technicalDetails;
    @ColumnInfo(name = "timeFrame")
    public  String timeFrame;
    @ColumnInfo(name = "description")
    public  String description;
    @ColumnInfo(name = "brand")
    public  String brand;
    @ColumnInfo(name = "year")
    public  int year;

    @ColumnInfo(name = "working")
    public  Boolean working;

    public itemCollection(String name, String Categorie,  String description, String brand, int year, Boolean working, String technicalDetails, String timeFrame) {
        this.id = id;
        this.name = name;
        this.Categorie = Categorie;
        this.technicalDetails = technicalDetails;
        this.description = description;
        this.brand = brand;
        this.year = year;
        this.timeFrame = timeFrame;
        this.working = working;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return this.id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setCategorie(String  Categorie) {
        this.Categorie = Categorie;
    }
    public String getCategorie() {
        return this.Categorie;
    }
   public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }
    public String getTechnicalDetails() {
        return this.technicalDetails;
    }
    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }
    public String getTimeFrame() {
        return this.timeFrame;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return this.description;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getBrand() {
        return this.brand;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return this.year;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }
    public Boolean getWorking() {
        return this.working;
    }



}
