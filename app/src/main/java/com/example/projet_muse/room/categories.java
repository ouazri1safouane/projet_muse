package com.example.projet_muse.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "categories")
public class categories {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "categorieId")
    private int id;
    @ColumnInfo(name = "categoCatego")
    private String catego ;

    public categories(String catego ) {
        this.id = id;
        this.catego = catego;

    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getCatego() {
        return this.catego;
    }
    public void setCatego(String catego) {
        this.catego = catego;
    }
}
