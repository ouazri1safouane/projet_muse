package com.example.projet_muse;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projet_muse.data.ItemResponse;
import com.example.projet_muse.data.MusInterface;
import com.example.projet_muse.room.categories;
import com.example.projet_muse.room.itemCollection;
import com.example.projet_muse.room.museRoomDatabase;
import com.example.projet_muse.room.version;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import static com.example.projet_muse.room.museRoomDatabase.databaseWriteExecutor;
public class museREPO {
    private LiveData<List<categories>> all;
    private LiveData<List<itemCollection>> allitem;
    private com.example.projet_muse.room.version version;
    private com.example.projet_muse.room.museDao museDao;
    private  MusInterface api;
    private  version it;


    private static volatile museREPO INSTANCE;
    private MutableLiveData<itemCollection> searchResults =
            new MutableLiveData<>();
    private List<itemCollection >findallIteml ;

    public synchronized static museREPO get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new museREPO(application);
        }

        return INSTANCE;
    }
    public museREPO(Application application) {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr")
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
        api = retrofit.create(MusInterface.class);
        museRoomDatabase db = museRoomDatabase.getDatabase(application);
        museDao = db.museDao();
        loadItems();
        loadCatego();


    }






    public void   loadVersion()  {


        api.getvERSION().enqueue(

                new Callback<Float>() {




                    @Override
                    public void onResponse(Call<Float> call, Response<Float> response) {
                         version versionn = new version(response.body().floatValue());

                        databaseWriteExecutor.execute(() -> {
                            museDao.deleteAllVersion();
                            museDao.insertVer(versionn);
                        });

                    }
                    @Override
                    public void onFailure(Call<Float> call, Throwable t) {


                    }


                });


    }
    public void   loadCatego()  {


        api.getCategoris().enqueue(

                new Callback<List<String>>() {


                    @Override
                    public void onResponse(Call<List<String>> call, Response<List<String>> response) {




                      if (!museDao.finallCategol().isEmpty() ) { databaseWriteExecutor.execute(() -> {
                            museDao.deleteAllcatego();

                        });}

                        for (String categon : response.body()) {
                            categories categori = new categories(categon);
                            databaseWriteExecutor.execute(() -> {
                                museDao.insetCatego(categori);
                            });
                        }


                    }
                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {

                    }


                });


    }


  public void loadItems() {




        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {


                    @Override

                    public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> r) {
                       String fill ;
                       String fill2 ;
                        if (!museDao.findallIteml().isEmpty() ) { databaseWriteExecutor.execute(() -> {
                            museDao.deleteAllItems();

                        });}


                        for ( Map.Entry<String, ItemResponse> categon : r.body().entrySet()) {

                            ItemResponse myitem = categon.getValue();
                            if (myitem.technicalDetails == null){ fill = "vide";}else{ fill = myitem.technicalDetails.toString();}

                            itemCollection item = new itemCollection(myitem.name,myitem.categories.toString(),myitem.description,myitem.brand,myitem.year,myitem.working,fill,myitem.timeFrame.toString());

                            databaseWriteExecutor.execute(() -> {

                                museDao.insertItem(item);

                            });


                        }

                    }


                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {

                    }


                });

    }
    public float getversion() {

            it =    museDao.findVersion();


        return it.getVersion();
    }

    public LiveData<List<itemCollection>> getalltiems( ) {

        allitem = museDao.findallItem();

return allitem;

    }


    public List<itemCollection> getitems() {
        findallIteml = museDao.findallIteml();
        return findallIteml;
    }
    public LiveData<List<categories>> allcategories() {

        all = museDao.finallCatego();



        return all;
    }

}
