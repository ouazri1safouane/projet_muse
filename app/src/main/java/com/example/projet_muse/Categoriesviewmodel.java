package com.example.projet_muse;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projet_muse.data.ItemResponse;
import com.example.projet_muse.room.categories;
import com.example.projet_muse.room.itemCollection;
import com.example.projet_muse.room.version;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Categoriesviewmodel extends AndroidViewModel  {

    museREPO REPO ;

    private LiveData<List<categories>> categoriesList;
    private LiveData<List<itemCollection>> Itemslist;
    private List<itemCollection> itemslist2;
    public Categoriesviewmodel(@NonNull Application application) throws IOException {
        super(application);

       REPO = new museREPO(application);
        categoriesList = REPO.allcategories();

        Itemslist = REPO.getalltiems();

        itemslist2 = REPO.getitems();
    }


    LiveData<List<categories>> getAllCategories() {
        return categoriesList;
    }
    LiveData<List<itemCollection>> getAllitems() {
        return Itemslist;
    }
   public  List<itemCollection> getNonObserved(){ return itemslist2;}

}
