package com.example.projet_muse;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.projet_muse.room.categories;
import com.example.projet_muse.room.itemCollection;
import com.example.projet_muse.room.version;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Locale;

public class FirstFragment extends Fragment {
    List<itemCollection> items;
    Categoriesviewmodel viewModel ;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter = new RecyclerAdapter();;
    RecyclerView recyclerView;
    float v ;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel= new ViewModelProvider(this).get(Categoriesviewmodel.class);
        recyclerView = getView().findViewById(R.id.myrecyler);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
        observerSetup();








    }
    private void observerSetup() {
        items =  viewModel.getNonObserved() ;
        adapter.setItemslist(items);
       /* viewModel.getAllCategories().observe(getViewLifecycleOwner(),
                categorieslistt -> adapter.setCategoriesList(categorieslistt));*/


    }

}