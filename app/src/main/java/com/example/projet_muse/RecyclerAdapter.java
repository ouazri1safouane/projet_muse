package com.example.projet_muse;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.projet_muse.room.categories;
import com.example.projet_muse.room.itemCollection;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{
   private int t = 0;
    int b ;
    private static final String TAG = "sbmitlistCategories";
    private List<String> catg = null;
    private  List<itemCollection> itemsList = new ArrayList<itemCollection>();
    private List<categories> categoriesList ;

    public void setCategoriesList(List<categories> categories) {
        categoriesList = categories;

    }
    public void setItemslist(List<itemCollection> items) {
        itemsList = items;

    }
    @NonNull
    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

       if ( t%2 == 0){
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card, parent, false);

            MyViewHolder viewHolder = new MyViewHolder(v);



            return viewHolder;
       }else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card2, parent, false);

            MyViewHolder viewHolder = new MyViewHolder(v);



            return viewHolder;
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyViewHolder holder, int position) {





       if ( t%2 == 0){

            TextView itembrand = holder.itembrand;
            TextView itemTitle = holder.itemTitle;
            TextView itemCategorie = holder.itemCategorie;
            itemTitle.setText(itemsList.get(position).getName());
            itembrand.setText(itemsList.get(position).getBrand());
            itemCategorie.setText(itemsList.get(position).getCategorie());
           t = t+1;

       }else {

            TextView itembrand2 = holder.itembrand2;
            TextView itemTitle2 = holder.itemTitle2;
            TextView itemCategorie2 = holder.itemCategorie2;
            itemTitle2.setText(itemsList.get(position).getName());
            itembrand2.setText(itemsList.get(position).getBrand());
            itemCategorie2.setText(itemsList.get(position).getCategorie());
           t = t+1;

        }


    }

    @Override
    public int getItemCount() {

if (itemsList.size() != 41)return 0;
        return  itemsList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itembrand;
        TextView itemCategorie;
        TextView itemTitle2;
        TextView itembrand2;
        TextView itemCategorie2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

                itemTitle = itemView.findViewById(R.id.nameCard);

                itembrand = itemView.findViewById(R.id.brandCard);
                itemCategorie = itemView.findViewById(R.id.categoriesCard);


            itemTitle2 = itemView.findViewById(R.id.nameCard2);

            itembrand2 = itemView.findViewById(R.id.brandCard2);
            itemCategorie2 = itemView.findViewById(R.id.categoriesCard2);







            int position = getAdapterPosition();
            itemView.setOnClickListener(new View.OnClickListener() {
@Override public void onClick(View v) {

                    int position = getAdapterPosition();
    itemCollection item = itemsList.get(position+1);
                    FirstFragmentDirections.ActionFirstFragmentToSecondFragment action =
                            FirstFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setIdArg(position);

                     Navigation.findNavController(v).navigate(action);
                    }
 });


        }
    }
}
