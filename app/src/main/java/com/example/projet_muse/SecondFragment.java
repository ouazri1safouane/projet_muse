package com.example.projet_muse;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.example.projet_muse.room.categories;
import com.example.projet_muse.room.itemCollection;

import java.util.List;

public class SecondFragment extends Fragment {
    TextView name;
    TextView catego;
    TextView annee;
    TextView frame;
    TextView etat;
    TextView description;
    TextView marque;
    TextView carac;
    Categoriesviewmodel viewModel ;

    private  List<itemCollection> items;
    private int i ;
    private RecyclerAdapter RecyclerAdapter;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);

    }

    @SuppressLint("SetTextI18n")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        viewModel= new ViewModelProvider(this).get(Categoriesviewmodel.class);
        super.onViewCreated(view, savedInstanceState);
        name = view.findViewById(R.id.name);
        catego = view.findViewById(R.id.catego);
        annee = view.findViewById(R.id.anne);
        frame = view.findViewById(R.id.frame);
        description = view.findViewById(R.id.desc);
        marque = view.findViewById(R.id.marque);
        carac = view.findViewById(R.id.details);
        etat = view.findViewById(R.id.fonctionement);
        SecondFragmentArgs args = SecondFragmentArgs.fromBundle(getArguments());
       i = args.getIdArg();
        observerSetup();


    }
    @SuppressLint("SetTextI18n")
    private void observerSetup() {


        items =  viewModel.getNonObserved() ;
                   name.setText(""+(items.get(i).getName())) ;
                    catego.setText(""+(items.get(i).getCategorie())) ;
                    annee.setText(""+(items.get(i).getYear())) ;
                   frame.setText(""+(items.get(i).getTimeFrame())) ;
                    description.setText(""+(items.get(i).getDescription())) ;
                    marque.setText(""+(items.get(i).getBrand())) ;
                   carac.setText(""+(items.get(i).getTechnicalDetails())) ;
                  if ( items.get(i).getWorking() == null )  {etat.setText("Non") ;}else{  etat.setText(""+(items.get(i).getWorking())) ;}






    }
}